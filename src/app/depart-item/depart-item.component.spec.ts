import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DepartItemComponent } from './depart-item.component';

describe('DepartItemComponent', () => {
  let component: DepartItemComponent;
  let fixture: ComponentFixture<DepartItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DepartItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DepartItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
