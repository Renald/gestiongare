import { Component, OnInit,Input } from '@angular/core';
import {DepartService } from '../services/depart.service';

@Component({
  selector: 'app-depart-item',
  templateUrl: './depart-item.component.html',
  styleUrls: ['./depart-item.component.scss']
})
export class DepartItemComponent implements OnInit {

 @Input() id!: number;
  @Input() Train!:string;
  @Input() Destination!: string;
  @Input() Horaire!: string;
  @Input() Statu!: string;
  @Input() IdStatu!: number;
  @Input() Type!:string;
  @Input() index!: number;
 
  @Input() Gare!: any[];


  constructor(private departService: DepartService) { }

  ngOnInit() {
   setInterval(
     () => {
       this.OnUpdate();
     }, 2000
   );

  }

  OnUpdate(){
    this.departService.OnUpdateStatu(this.index, this.id);
  }

}
