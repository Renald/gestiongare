import { Component, OnInit } from '@angular/core';
import {ArriveeService} from '../services/arrivee.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-arrivee',
  templateUrl: './arrivee.component.html',
  styleUrls: ['./arrivee.component.scss']
})
export class ArriveeComponent implements OnInit {

  arriveeSubsciption!: Subscription;
  arrivee!:any[];
  constructor(private arriveeService : ArriveeService ) { }

  ngOnInit() {
    this.arriveeService.appelAPI();
    this.arriveeSubsciption = this.arriveeService.arriveeSubject.subscribe(
      (arrivee : any[]) =>{
          this.arrivee = arrivee;
      }
    );
    setInterval(
      ()=>{
        this.OnUpdate();
      }, 10000
    );
    setInterval(
      ()=>{
        this.OnUpdate();
      }, 5000
    );
   
  }


   
 

  OnUpdate(){
    this.arriveeService.Update();
  }


}
