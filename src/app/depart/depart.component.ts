import { Component, OnInit } from '@angular/core';
import {DepartService} from '../services/depart.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-depart',
  templateUrl: './depart.component.html',
  styleUrls: ['./depart.component.scss']
})
export class DepartComponent implements OnInit {

  departSubsciption!: Subscription;
  depart!:any[];
  constructor(public departService : DepartService ) { }

  ngOnInit() {
    this.departService.appelAPI();
    this.departSubsciption = this.departService.departSubject.subscribe(
      (depart : any[]) =>{
          this.depart = depart;
      }
    );
   
  setInterval(
    ()=>{
      this.OnUpdate();
    }, 5000
  );
   
  }

  OnUpdate(){
    this.departService.Update();
  }

}
