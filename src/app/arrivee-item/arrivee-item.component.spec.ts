import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArriveeItemComponent } from './arrivee-item.component';

describe('ArriveeItemComponent', () => {
  let component: ArriveeItemComponent;
  let fixture: ComponentFixture<ArriveeItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArriveeItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArriveeItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
