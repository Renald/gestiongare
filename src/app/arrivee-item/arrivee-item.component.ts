import { Component, OnInit,Input } from '@angular/core';
import {ArriveeService } from '../services/arrivee.service';

@Component({
  selector: 'app-arrivee-item',
  templateUrl: './arrivee-item.component.html',
  styleUrls: ['./arrivee-item.component.scss']
})
export class ArriveeItemComponent implements OnInit {

  @Input() id!: number;
  @Input() Train!:string;
  @Input() Destination!: string;
  @Input() Horaire!: string;
  @Input() Statu!: string;
  @Input() IdStatu!: number;
  @Input() Type!:string;
  @Input() index!: number;
 
  @Input() Gare!: any[];

  constructor() { }

  ngOnInit() {
   

  }

}
