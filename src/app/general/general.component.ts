import { Component, OnInit,Directive, ComponentRef } from '@angular/core';
import { HttpClient, HttpClientModule, HttpHandler } from '@angular/common/http';
import {DepartService} from '../services/depart.service';
import {ArriveeService} from '../services/arrivee.service';
import { BrowserModule } from '@angular/platform-browser';
import { Observable, Subscription, TimeoutError } from 'rxjs';
import {Compiler, Injector, ViewChild, NgModule, NgModuleRef,
  ViewContainerRef, Injectable,AfterViewInit, ViewRef} from '@angular/core';
import { TestService } from '../services/test.service';
import {ActivatedRoute } from '@angular/router';
import 'rxjs/add/observable/interval';
import { map, share } from 'rxjs/operators';
import { formatDate } from "@angular/common";
import { environment } from 'src/environments/environment';
import { ConfigurationHelper } from 'src/environments/configuration-helper';




@Component({
  selector: 'app-general',
  //templateUrl : './general.component.html',
  template: '<ng-template #dynamicTemplate ></ng-template>',
  styleUrls: ['./general.component.scss']
})



export class GeneralComponent  implements OnInit {

  @ViewChild('dynamicTemplate', {read: ViewContainerRef}) dynamicTemplate!: ViewContainerRef;

  public component!: ComponentRef<any>;


  //mytemplate = '../assets/image/general.component.html';
  //mytemplate ='<html-oultet [html] ="./src/app/general/general.component.html"></html-outlet>';
  //mytemplate : string ='<div class="col-xs-12"><div class=" d-flex justify-content-start"><div class="ps-5 w-100"><h4>Départ</h4><ul class="list-group"><li   *ngFor="let departs of depart;let i = index" class="list-group-item  d-flex justify-content-start" ><div ><h1>{{departs.type}} {{test}}</h1></div><div class="ps-5"><h4>{{ departs.train }}</h4><p>{{departs.destination}}</p><p *ngIf="departs.gare_desc.length >0">Gare désservis : <span *ngFor="let gares of departs.gare_desc">{{gares.nom}} -- </span>    </p> <p>{{departs.statu.etat}}</p></div></li></ul></div></div></div>';
  //mytemplate : string ='<h4>{{test}}</h4>';
  mytemplate!:string;
  mytemplateArray!: any[];
  mytemplateSubscription!: Subscription;
  departSubsciption!: Subscription;
  arriveeSubsciption!: Subscription;
  addDepartSubsciption!: Subscription;
  depart!:any[];
  arrivee!:any[];

  quai!:string;
  quai_type!:string;
  quai_train!:string;
  quai_statu!:string;

  templateCurrent!: string;

departRefSubscription!: Subscription;
departRef!: any[];

  addDepart!: any[];
 
  time!: Observable<Date>;
  clock!:string;
 
  interval!: ReturnType<typeof setTimeout>;



    

 constructor(private _compiler: Compiler,
  private _injector: Injector,
  private _m: NgModuleRef<any>,
  private departService : DepartService,
  private arriveeService : ArriveeService,
  private httpClient : HttpClient,
  private route: ActivatedRoute,
  private configHelper: ConfigurationHelper
  ){
 
 }

 
 Compilercomp(mytemplate:string, id: string) {
 
  let tmp!: any[];

  
  const tmpCmp = Component({
    template: mytemplate
         })(class{});
 
  const tmpModule = NgModule({declarations: [tmpCmp],imports: [
    BrowserModule]})(class {
             });

    this._compiler.compileModuleAndAllComponentsAsync(tmpModule)
    .then((factories) => {
      
      const f = factories.componentFactories[0];
      const cmpRef = this.dynamicTemplate.createComponent(f);
      this.component = cmpRef;
     
      this.time =Observable.interval(1000).pipe(
        map(tick => new Date()),share());
      
        
    
    
          if(id === '2' || id ==='8')
               this.Depart();
          if(id === '5')
                this.Arrivee();

      
    

    });

  }

  Arrivee(){
    this.arriveeService.appelAPI();
    this.arriveeSubsciption = this.arriveeService.arriveeSubject.subscribe(
      (arrivee : any[]) =>{
    
       this.arrivee = arrivee;
       this.component.instance.arrivee = arrivee;
       
  
        }
     
     );

  }

  Depart(){
        this.departService.appelAPI();
        this.departSubsciption = this.departService.departSubject.subscribe(
          (depart : any[]) =>{
        
           this.depart = depart;
          
           this.component.instance.depart = depart;
           this.component.instance.quai_type= this.depart[0].type;
              this.component.instance.quai_train = this.depart[0].train;
              this.component.instance.quai_statu = this.depart[0].statu.etat;
            }
         
         );

         this.interval = setInterval(
           () => {
                 this.departService.Update();
              },2000
           );

         this.time.subscribe(
            (timer) =>{
            if(this.depart){
              this.departService.initCounDown(timer,0);
              this.quai = this.departService.getQuaiclock();
              this.component.instance.quai = this.quai;
              this.component.instance.quai_type= this.depart[0].type;
              this.component.instance.quai_train = this.depart[0].train;
              this.component.instance.quai_statu = this.depart[0].statu.etat;
              //console.log(this.allocateTimeUnits(timeDifference));
             
             // const clock  =formatDate(timer, 'HH:mm:ss','en-US');
              
            
              //this.departService.VerifClock(clock);
              
            
                //this.departRefSubscription.unsubscribe();
              }
            }
           );


         

  }

    //depart.push('[{"id": 1, "train": "Q28","type": "TER", "destination": "Marseille","horaire": "12h00"}]');
    
    //this.depart.push(this.depart);
   
   clearTemplate(){
    if(this.arriveeSubsciption)
            this.arriveeSubsciption.unsubscribe();
           if(this.departSubsciption)
                 this.departSubsciption.unsubscribe();
            if(this.component)
                this.component.destroy();
            clearInterval(this.interval);
   }
    
  
 ngOnInit() {
        
  this.departService.appelAPIStatuGare();
           
  setInterval(
    () =>{
        
        this.departService.appelAPIStatuGare();
        
        
    },5000
  );


  this.departService.statuGareSubject.subscribe(
    (statuGare) => {
     
        for (let statuGares of statuGare){
            if(statuGares.fire.libelle === 'NOOK')
                if(this.templateCurrent != 'fire'){
                  this.clearTemplate();
                  console.log(this.templateCurrent);
                  this.templateCurrent = 'fire';
                    if(this.mytemplateSubscription)
                        this.mytemplateSubscription.unsubscribe(); 
                  const id = '6';
                  this.departService.appelAPITemplate();
                  this.generatetemplate(id);
                }
                if(statuGares.fire.libelle != 'NOOK')   
                   if(this.templateCurrent != 'nofire'){
                    this.clearTemplate();
                    console.log(this.templateCurrent);
                      this.templateCurrent = 'nofire';
                      if(this.mytemplateSubscription)
                          this.mytemplateSubscription.unsubscribe(); 
                       const id= this.route.snapshot.params['id'];
                       this.departService.appelAPITemplate();
                       
                       this.generatetemplate(id);
                }
        }
    }
);


 
  }


  generatetemplate(id : string){
  this.mytemplateSubscription = this.departService.mytemplateSubject.subscribe(
      (mytemplate) =>{
       
       const  src = this.departService.getTemplateById(+id)!.title.src
        if(this.mytemplate != src){
             this.mytemplate = src;
               this.clearTemplate();
               this.Compilercomp(this.mytemplate,id);
        }

      } 
  );

    }       

   //this.Compilercomp(this.mytemplate);
   
   /*setTimeout(() => {
    this.component.destroy();
    this.departSubsciption.unsubscribe();
     clearInterval(this.interval); 
    setTimeout(() => {
      this.Compilercomp(this.mytemplate);
        }, 1000);
        },
    5000);*/
 
      
  
    
}

