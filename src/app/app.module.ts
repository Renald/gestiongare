import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { RouterModule } from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AffichageComponent } from './affichage/affichage.component';
import {DepartService} from './services/depart.service';

import {TestService} from './services/test.service';
import {ArriveeService} from './services/arrivee.service';
import { DepartComponent } from './depart/depart.component';
import { DepartItemComponent } from './depart-item/depart-item.component';
import { HttpClientModule } from '@angular/common/http';
import { ArriveeComponent } from './arrivee/arrivee.component';
import { ArriveeItemComponent } from './arrivee-item/arrivee-item.component';
import { FireComponent } from './fire/fire.component';
import { Routes } from '@angular/router';
import { COMPILER_OPTIONS, CompilerFactory, Compiler } from '@angular/core';
import { JitCompilerFactory } from '@angular/platform-browser-dynamic';
import { GeneralComponent } from './general/general.component';

import { environment } from 'src/environments/environment';
import { ConfigurationHelper } from 'src/environments/configuration-helper';




const appRoute :Routes = [
  {path : 'affichage',component: AffichageComponent},
  {path : 'fire',  component: FireComponent},
  {path: 'general/:id', component: GeneralComponent},
  {path: '**', redirectTo: 'general/5' }
  ];
  


@NgModule({
  declarations: [
    AppComponent,
    AffichageComponent,
    DepartComponent,
    DepartItemComponent,
    ArriveeComponent,
    ArriveeItemComponent,
    FireComponent,
    GeneralComponent
   

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    RouterModule.forRoot(appRoute)
  ],
  providers: [
    DepartService,
    ArriveeService,
    TestService,
    ConfigurationHelper,
    { provide: COMPILER_OPTIONS, useValue: {}, multi: true },
    { provide: CompilerFactory, useClass: JitCompilerFactory, deps: [COMPILER_OPTIONS] },
    { provide: Compiler, useFactory: createCompiler, deps: [CompilerFactory]},
     { provide: APP_INITIALIZER, useFactory: initConfig, deps: [ConfigurationHelper], multi: true }
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function initConfig(configService: ConfigurationHelper) {
  return () =>  configService.load( 'assets/config/config.json', 'webapi');
     
 
  
}


export function createCompiler(compilerFactory: CompilerFactory) {
  return compilerFactory.createCompiler();
}



