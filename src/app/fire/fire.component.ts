
import {Component,Compiler, Injector, ViewChild, NgModule, NgModuleRef,
  ViewContainerRef, Injectable,AfterViewInit, OnInit, ViewRef} from '@angular/core';


import {TestService} from '../services/test.service';

@Component({
  selector: 'app-fire',
  template: '<ng-container #dynamicTemplate></ng-container>'
  
})



export class FireComponent implements  OnInit  {

  @ViewChild('dynamicTemplate', {read: ViewContainerRef}) dynamicTemplate!: ViewContainerRef;
  mytemplate : string ='<h4>{{getTest()}}</h4>';


  constructor(private _compiler: Compiler,
    private _injector: Injector,
    private _m: NgModuleRef<any>
    ) {}
   
    load(){
      console.log('test');
    } 
    
 Compilercomp(mytemplate:string) {
 

  const tmpCmp = Component({
    template: mytemplate
         })(class { });
 
  const tmpModule = NgModule({declarations: [tmpCmp]})(class {
             });

  this._compiler.compileModuleAndAllComponentsAsync(tmpModule)
    .then((factories) => {
      const f = factories.componentFactories[0];
      const cmpRef = f.create(this._injector, [], null, this._m);
      cmpRef.instance.name = 'dynamic';
      this.dynamicTemplate.insert(cmpRef.hostView);
     
    });
}


  ngOnInit() {
    this.Compilercomp(this.mytemplate);
    
  }

}




