

import { HttpClient, HttpClientModule, HttpHandler } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';
import {Observable} from 'rxjs/Observable';
import { formatDate } from "@angular/common";
import { environment } from 'src/environments/environment';




@Injectable()
export class ArriveeService {

    arriveeSubject = new Subject<any[]>();
     arrivee! : any[];

     arriveeRefSubject = new Subject<any[]>();
     arriveeRef!: any[];

     statuGare!: any[];
     statuGareSubject= new Subject<any[]>();
    
   mytemplateSubject = new Subject<any[]>();
   mytemplate!: any[];

    constructor(private httpClient : HttpClient) { 
       
    }

    emitStatuGareSubject(){
        this.statuGareSubject.next(this.statuGare.slice());
    }
    emitArriveeRefSubject(){
        this.arriveeRefSubject.next(this.arriveeRef.slice());
    }

    emitArriveeSubject(){
        this.arriveeSubject.next(this.arrivee.slice());
    }

    emitMyTemplateSubject(){
        this.mytemplateSubject.next(this.mytemplate.slice());
    }
    

    appelAPI(){
        this.httpClient.get<any[]>(environment.webapi+'/API/arrivee')
                .subscribe(
                 (response) =>{
                 this.arrivee = response;
                 this.arriveeRef = response;
                 this.emitArriveeSubject();
                 this.emitArriveeRefSubject();
              }
             );
    }

  /*  appelTemplateAPI(){
        this.httpClient.get<any[]>('http://192.168.1.70:1800/API/template')
            .subscribe(
               (response) =>{
                    for(let response of response)
               } 
            )
    }*/


   

    getarriveeRefById(id : number){
        const arrivee = this.arrivee.find(
            (arrivee ) => {
                if( arrivee.id === id)
                return arrivee.id;
            }
        );
        return arrivee;
    }


    getarriveeById(id : number){
        const arrivee = this.arrivee.find(
            (arrivee ) => {
                
                return arrivee.id === id;
            }
        );
        return arrivee;
    }

    getResponseById(id:number, response: any[]){
        const responseC = response.find(
            (response) =>{
                return response.id === id
            }
        );
        return responseC; 
    }


    getTemplateById(id : number){
        const template = this.mytemplate.find(
            (template ) => {
                return template.id === id;
            }
        );
        //console.log(template.title);
        return template;
    }

    appelAPIStatuGare(){
        this.httpClient.get<any[]>(environment.webapi+'/API/statugare')
            .subscribe(
                (response) => {
                    this.statuGare = response;
                    this.emitStatuGareSubject();

                }
            )
    }

    appelAPITemplate(){
      
                this.httpClient.get<any[]>(environment.webapi+'/API/template')
                    .subscribe(
                             (response) =>{
              
                                        
                                                this.mytemplate = response; 
                                                this.emitMyTemplateSubject();
                                            }
                                      );
                               
                       

                         
             }


    OnUpdateStatu(index : number,id: number){
        this.httpClient.get<any[]>(environment.webapi+'/API/arrivee/'+ id)
        .subscribe(
            (response) => {
              for (let responses of response)
                if(this.arrivee[index].id === responses.id)
                    this.arrivee[index].statu = responses.statu;
            }
        );
        
    }

   


    Update(){
        this.httpClient.get<any[]>(environment.webapi+'/environment.webapi/arrivee')
        .subscribe(
         (response) =>{
            this.arriveeRef = response;
           
            for (let arrivees of this.arrivee){
                
                if(!this.getResponseById(arrivees.id,response))
               this.arrivee.splice(arrivees);
            }
           
           for(let responses of response){
               if(!this.getarriveeById(responses.id) && !responses.planif)
                this.arrivee.push(responses);
                
           }
               
         this.emitArriveeSubject();
         this.emitArriveeRefSubject();
        }
        );

    }

    initCounDown(timer : Date){
        for (let arrivees of this.arrivee){
            const dateNow = formatDate(new Date(),'MMM dd yyyy','en-US');
            const date = new Date(dateNow+' 11:23:00');
            const timeDifference =   date.getTime() - timer.getTime();
            const clock  =formatDate(timeDifference, 'HH:mm:ss','en-US');
            arrivees.horaire = clock;
        }
        this.emitArriveeSubject();
    }

    VerifClock(clock : string){
        for (let arrivees of this.arriveeRef){
                  
            if(arrivees.planif && arrivees.planif === clock)
                  this.UpdateRef(arrivees.id,arrivees);
               }
        
             }


    UpdateRef(id : number, arriveeref: any[]){
            if(!this.getarriveeRefById(id))
                this.arrivee.push(arriveeref);
    }
}   