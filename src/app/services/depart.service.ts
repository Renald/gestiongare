

import { HttpClient, HttpClientModule, HttpHandler } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';
import {Observable} from 'rxjs/Observable';
import { formatDate } from "@angular/common";
import { environment } from 'src/environments/environment';



@Injectable()

export class DepartService {

    milliSecondsInASecond:number = 1000;
    hoursInADay: number = 24;
    minutesInAnHour:number = 60;
    SecondsInAMinute:number  = 60;

    Quaiclock!: string;

    public timeDifference!: number;
    public secondsToDday!:number;
    public minutesToDday!:number;
    public hoursToDday!:number;
    public daysToDday!:number;
    
    public secondString!:string;
    public minuteString!:string;
    public hourString!:string;

    departSubject = new Subject<any[]>();
     depart! : any[];

     departRefSubject = new Subject<any[]>();
     departRef!: any[];

     statuGare!: any[];
     statuGareSubject= new Subject<any[]>();
    
   mytemplateSubject = new Subject<any[]>();
   mytemplate!: any[];


    constructor(private httpClient : HttpClient) { 
       
    }

    emitStatuGareSubject(){
        this.statuGareSubject.next(this.statuGare.slice());
    }
    emitDepartRefSubject(){
        this.departRefSubject.next(this.departRef.slice());
    }

    emitDepartSubject(){
        this.departSubject.next(this.depart.slice());
    }

    emitMyTemplateSubject(){
        this.mytemplateSubject.next(this.mytemplate.slice());
    }
    

    appelAPI(){
        
        this.httpClient.get<any[]>(environment.webapi+'/API/depart')
                .subscribe(
                 (response) =>{
                 this.depart = response;
                 this.departRef = response;
                 this.emitDepartSubject();
                 this.emitDepartRefSubject();
              }
             );
    }

  /*  appelTemplateAPI(){
        this.httpClient.get<any[]>('http://192.168.1.70:1800/API/template')
            .subscribe(
               (response) =>{
                    for(let response of response)
               } 
            )
    }*/


   

    getDepartRefById(id : number){
        const depart = this.depart.find(
            (depart ) => {
                if( depart.id === id)
                return depart.id;
            }
        );
        return depart;
    }


    getDepartById(id : number){
        const depart = this.depart.find(
            (depart ) => {
                
                return depart.id === id;
            }
        );
        return depart;
    }

    getResponseById(id:number, response: any[]){
        const responseC = response.find(
            (response) =>{
                return response.id === id
            }
        );
        return responseC; 
    }


    getTemplateById(id : number){
        const template = this.mytemplate.find(
            (template ) => {
                return template.id === id;
            }
        );
        //console.log(template.title);
        return template;
    }

    appelAPIStatuGare(){
        
        this.httpClient.get<any[]>(environment.webapi+'/API/statugare')
            .subscribe(
                (response) => {
                    this.statuGare = response;
                    this.emitStatuGareSubject();

                }
            )
    }

    appelAPITemplate(){
      
                this.httpClient.get<any[]>(environment.webapi+'/API/template')
                    .subscribe(
                             (response) =>{
              
                                        
                                                this.mytemplate = response; 
                                                this.emitMyTemplateSubject();
                                            }
                                      );
                               
                       

                         
             }


    OnUpdateStatu(index : number,id: number){
        this.httpClient.get<any[]>(environment.webapi+'/API/depart/'+ id)
        .subscribe(
            (response) => {
              for (let responses of response)
                if(this.depart[index].id === responses.id)
                    this.depart[index].statu = responses.statu;
            }
        );
        
    }

   


    Update(){
        this.httpClient.get<any[]>(environment.webapi+'/API/depart')
        .subscribe(
         (response) =>{
            this.departRef = response;
           
            for (let departs of this.depart){
                
                if(!this.getResponseById(departs.id,response))
               this.depart.splice(departs);
            }
           
           for(let responses of response){
               if(!this.getDepartById(responses.id) && !responses.planif)
                this.depart.push(responses);
                
           }
               
         this.emitDepartSubject();
         this.emitDepartRefSubject();
        }
        );

    }

    getQuaiclock(){
        return this.Quaiclock;
    }

    initCounDown(timer : Date,index : number){
        
            const dateNow = formatDate(new Date(),'MMM dd yyyy','en-US');
            const date = new Date(dateNow+' '+this.depart[index].horaire);
            const timeDifference =   date.getTime() - timer.getTime();
          console.log(this.depart[index].horaire);
            const clock  =this.allocateTimeUnits(timeDifference);
           
           this.Quaiclock = clock;
            if(timeDifference <= 0){
            this.Quaiclock = "00:00:00";
            this.depart[index].statu.etat ='En Approche';
            }
        this.emitDepartSubject();
    }

    VerifClock(clock : string){
        for (let departs of this.departRef){
                  
            if(departs.planif && departs.planif === clock)
                  this.UpdateRef(departs.id,departs);
               }
        
             }


    UpdateRef(id : number, departref: any[]){
            if(!this.getDepartRefById(id))
                this.depart.push(departref);
    }


    allocateTimeUnits (timeDifference: number):string {
        this.secondsToDday = Math.floor((timeDifference) / (this.milliSecondsInASecond) % this.SecondsInAMinute);
        this.minutesToDday = Math.floor((timeDifference) / (this.milliSecondsInASecond * this.minutesInAnHour) % this.SecondsInAMinute);
        this.hoursToDday = Math.floor((timeDifference) / (this.milliSecondsInASecond * this.minutesInAnHour * this.SecondsInAMinute) % this.hoursInADay);
        this.daysToDday = Math.floor((timeDifference) / (this.milliSecondsInASecond * this.minutesInAnHour * this.SecondsInAMinute * this.hoursInADay));
  
        if(this.secondsToDday < 10)
            this.secondString='0'+this.secondsToDday;
        else   
            this.secondString = String(this.secondsToDday);
        
        
        if(this.minutesToDday < 10)
            this.minuteString='0'+this.minutesToDday;
        else   
            this.minuteString = String(this.minutesToDday);


            if(this.hoursToDday < 10)
            this.hourString='0'+this.hoursToDday;
        else   
            this.hourString = String(this.hoursToDday);

        return this.hourString+":"+this.minuteString+":"+this.secondString;
      }   
          
}   