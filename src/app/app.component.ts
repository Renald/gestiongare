import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'GestionGare';

  constructor(private router: Router,private httpClient: HttpClient)  {}

  ngOnInit(){
   /* setInterval(
      () => {
        this.OnLoad();
      }, 5000
    );*/
    
  }

  OnLoad(){
    this.httpClient
    .get<any[]>('http://192.168.1.70:1800/API/statugare')
    .subscribe(
     (response) =>{
      for (let responses of response)
        if(responses.fire.id === 2)
        this.router.navigate(['/fire']);
        else
        this.router.navigate(['/affichage']);
    }

    );
    }
}
