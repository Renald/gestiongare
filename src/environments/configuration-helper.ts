import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from './environment';

@Injectable()
export class ConfigurationHelper {

    private config!: any;

    constructor(private http: HttpClient) { }

    // Charger les informations du fichier de configuration
    public load( path: string, key :string): Promise<any>
    {
        return new Promise((resolve, reject) => {
            
                 this.http.get(path).subscribe(
                     (data) => {
                    this.config = data;
                    environment.webapi = this.getApi(key);
                    resolve(true);
                });
            });
       
    }

    public getApi(key : string ): string {
        return this.config[key];
    }
}   
